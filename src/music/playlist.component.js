angular.module('music')
  .directive('appPlaylist', function () {

    return {
      scope:{
        'title':'@title'
      },
      templateUrl: 'dist/music/playlist.tpl.html'
    }
  })