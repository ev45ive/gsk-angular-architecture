angular.module('music')
  // app-playlists-view
  .directive('appPlaylistsView', function () {
    return {
      templateUrl: 'dist/music/playlists-view.tpl.html',
      controller: function ($scope) {

        $scope.playlistsList = [{
            name: 'Angular top20!',
            favourite: true,
            color: '#ff0000'
          },
          {
            name: 'Angular Hits!',
            favourite: false,
            color: '#00ff00'
          },
          {
            name: 'Best of Angular!',
            favourite: true,
            color: '#0000ff'
          },
        ]

        $scope.select = function(item) {
          $scope.selectedPlaylist = item
        }

        $scope.selectedPlaylist = $scope.playlistsList[0]
      }
    }
  })


/* // 1.7
angular.module('music')
  .component('app-playlist-view', {
    template: '<div>I am a component</div>',
    controller: function () {
      this.playlist = {}
    }
  }) */