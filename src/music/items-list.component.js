angular.module('music')
  .directive('appItemsList', function () {
    return {
      scope: {
        'title': '@', // title:'@title',
        'itemsList': '=items',
        'selected': '=',
        'onSelect': '&onSelect'
      },
      templateUrl: 'dist/music/items-list.tpl.html',
      controller: function ($scope) {

        $scope.select = function (item) {

          $scope.onSelect({
            selected: item,
            message: 'Pancakes!'
          })
        }

      }
    }
  })