angular.module('music')
  .directive('appPlaylistDetails', function () {
    return {
      scope: {
        // bind attribute playlist as variable playlist
        playlistDetails: '=playlist'
      },
      templateUrl: 'dist/music/playlist-details.tpl.html',
      controller: function ($scope) {

        // $scope.playlistDetails = $scope.playlist

        // $scope.playlistDetails = {
        //   name: 'ANgular top20!',
        //   favourite: true,
        //   color: '#ff0000'
        // }

      }
    }
  })