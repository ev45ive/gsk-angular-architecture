angular.module('search')
  .directive('appAlbumsGrid', function () {
    return {
      scope: {
        albums: '=' // 'albums':'=albums'
      },
      templateUrl: 'dist/search/albums-grid.tpl.html',
      controller: function ($scope) {
        // $scope.albums = []
      }
    }
  })