// app.module.js 

angular.module('app.module', ['ngRoute', 'music', 'search'])
  // Configuration Phase
  .config(function (SecurityProvider) {

    SecurityProvider.setConfig({
      url: 'https://accounts.spotify.com/authorize',
      client_id: 'dbdcbeb0292941aebfe346bde8aea36c',
      response_type: 'token',
      redirect_uri: 'http://localhost:3000/'
    })

  })
  // Bootstrap / Run Phase
  .run(function (Security) {
    Security.getToken()
  })

  .config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
      when('/playlists', {
        template: '<app-playlists-view></app-playlists-view>'
      }).
      when('/search', {
        template: '<app-search-view></app-search-view>'
      }).
      when('/album/:id', {
        template: '<app-album></app-album>'
      }).
      otherwise('/playlists');
    }
  ]);

// .config(function(OtherProvider){
//   OtherProvider.setConfig(config)
// })