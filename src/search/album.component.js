angular.module('app.module')
  .directive('appAlbum', function (MusicSearch) {
    return {
      scope: {

      },
      templateUrl: 'dist/search/album.tpl.html',
      controller: function ($scope, $routeParams) {
        MusicSearch.getAlbum($routeParams.id).then(function (album) {
          $scope.album = album
          $scope.tracks = album.tracks.items
        })


        $scope.play = function (track) {
          $scope.selected = track
          $scope.url = (track.preview_url)
        }
      }
    }
  })