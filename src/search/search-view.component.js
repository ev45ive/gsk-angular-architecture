angular.module('search')
  // <app-search-view></app-search-view>
  .directive('appSearchView', function (MusicSearch) {
    return {
      templateUrl: 'dist/search/search-view.tpl.html',
      controller: function ($scope) {

        $scope.albums = [{
          name: "Awesome album",
          images: [{
            url: 'http://placekitten.com/300/300'
          }]
        }]

        $scope.search = function (query) {

          MusicSearch.search(query)
            .then(function (albums) {
              $scope.albums = albums
            })
        }


      }
    }
  })