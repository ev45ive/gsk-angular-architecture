angular.module('search')
  .service('MusicSearch', function ($http, Security) {

    var url = 'https://api.spotify.com/v1/search'

    this.search = function (query) {
      return $http.get(url, {
        headers: {
          Authorization: 'Bearer ' + Security.getToken()
        },
        params: {
          q: query,
          type: 'album',
          limit: 20
        }
      }).then(function (resp) {
        return resp.data.albums.items
      }, function (error) {
        if(error.status == 401){
          Security.authorize()
        }
      })
    }

    this.getAlbum = function (id) {
      return $http.get('https://api.spotify.com/v1/albums/'+id, {
        headers: {
          Authorization: 'Bearer ' + Security.getToken()
        },
      }).then(function (resp) {
        console.log(resp)
        return resp.data//.albums.items
      }, function (error) {
        if(error.status == 401){
          Security.authorize()
        }
      })
    }
  })