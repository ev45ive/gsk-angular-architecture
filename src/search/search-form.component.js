angular.module('search')
  .directive('appSearchForm', function () {

    return {
      scope: {
        onSearch: '&'
      },
      templateUrl: 'dist/search/search-form.tpl.html',
      controller: function ($scope) {

        $scope.search = function (query) {
          $scope.onSearch({
            query: query
          })
        }
      }
    }
  })