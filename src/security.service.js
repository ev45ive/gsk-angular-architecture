angular.module('app.module')
  .provider('Security', function () {

    var config = {};

    // Configuration Phase
    return {

      setConfig: function (c) {
        config = c;
      },

      // Bootstrap / Run Phase
      $get: function () {
        return {
          token: '',

          authorize: function () {
            sessionStorage.removeItem('token')
            location.replace(
              config.url + '?' +
              'client_id=' + config.client_id +
              '&response_type=' + config.response_type +
              '&redirect_uri=' + config.redirect_uri
            )
          },

          getToken: function () {
            this.token = JSON.parse(sessionStorage.getItem('token'))

            if (!this.token && location.hash) {
              var match = location.hash.match(/#access_token=([^&]+)/)
              this.token = match && match[1]

              sessionStorage.setItem('token', JSON.stringify(this.token))
            }

            if (!this.token) {
              this.authorize()
            }
            return this.token
          }
        }
      }
    }
  })