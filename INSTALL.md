# Install Global tools
npm install -g http-server json-server gulp-cli --save-dev

# Install local Gulp (and plugins)
npm install --save-dev gulp gulp-concat gulp-sourcemaps

# Gulp configuration
./gulpfile.js

gulp --help
gulp --tasks
gulp <taskname>

# Vendor libraries
npm i --save angular bootstrap

# Browser Sync
npm install browser-sync --save-dev