// gulpfile.js
var gulp = require('gulp')
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

var vendorsJS = [
  'node_modules/angular/angular.js',
  'node_modules/angular-route/angular-route.js'
]
var vendorCSS = ['node_modules/bootstrap/dist/css/bootstrap.css']

var sourceJS = [
  '!src/**.test.js',
  'src/**/*.module.js',
  'src/**/*.js'
]
var sourceCSS = [
  'src/**/*.css'
]

gulp.task('bundle-js', function () {
  return gulp.src([].concat(
      vendorsJS,
      sourceJS
    ))
    .pipe(sourcemaps.init())
    .pipe(concat('bundle.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist'))
})

gulp.task('bundle-css', function () {
  return gulp.src([].concat(
      vendorCSS,
      sourceCSS
    ))
    .pipe(concat('bundle.css'))
    .pipe(gulp.dest('dist'))
})

var browserSync = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
});

// Watch for changes
gulp.task('watcher', function () {
  // var gulpWatcher = require('gulp-watcher')
  // gulpWatcher.watch(sourceCSS, ['bundle-css'])
  // .on('change', browserSync.reload);

  gulp.watch(sourceCSS, ['bundle-css'])
    .on('change', browserSync.reload);

  gulp.watch(sourceJS, ['bundle-js'])
    .on('change', browserSync.reload);

  gulp.watch('src/**/*.tpl.html', ['copy-tpl'])
    .on('change', browserSync.reload);
})

// Copy tpl.html templates to /dist
gulp.task('copy-tpl', function () {
  return gulp.src('src/**/*.tpl.html')
    .pipe(gulp.dest('dist'))
})

gulp.task('serve', ['bundle', 'watcher', 'browser-sync'])

gulp.task('bundle', ['bundle-css', 'bundle-js', 'copy-tpl'])

gulp.task('default', ['bundle'])